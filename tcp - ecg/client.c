/*---------------------------------------------------------------------------*/
/*                                                                           */
/* FILE:    client.c                                                         */
/*                                                                           */
/* PURPOSE: This is a skeleton program to demonstrate how you would write a  */
/*          a TCP Client application. This program connects to an established*/
/*          TCP server and communicates via a user interface panel.  This    */
/*          sample only communicates with one server, but illustrates how to */
/*          implement a callback function to respond to TCP events.          */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Include files                                                             */
/*---------------------------------------------------------------------------*/
#include "toolbox.h"
#include <formatio.h>
#include <analysis.h>
#include <cvirte.h>
#include <stdio.h>
#include <stdlib.h>
#include <tcpsupp.h>
#include <string.h>
#include <utility.h>
#include <userint.h>
#include "client.h"

/*---------------------------------------------------------------------------*/
/* Macros						                                             */
/*---------------------------------------------------------------------------*/
#define tcpChk(f) if ((g_TCPError=(f)) < 0) {ReportTCPError(); goto Done;} else
#define TOTAL 7680
#define NUMERO_CANALES 8
#define PAQUETE 19

/*---------------------------------------------------------------------------*/
/* Internal function prototypes                                              */
/*---------------------------------------------------------------------------*/
int CVICALLBACK ClientTCPCB (unsigned handle, int event, int error,
                             void *callbackData);
static void ReportTCPError (void);

/*---------------------------------------------------------------------------*/
/* Module-globals                                                            */
/*---------------------------------------------------------------------------*/
static unsigned int g_hconversation;
static int          g_hmainPanel;
static int			g_connected = 0;
static int			g_TCPError = 0;
static double dVector[TOTAL]={0};
static double dParte[2000]={0};
static double dRuido[2000]={0};
//double dLectura


void addSignal(double dSignal[], int iLongitud)
{
	static int iIndice=0;
	
	for (int i=0; i<iLongitud; i++)
	{
		dVector[iIndice++]=dSignal[i];
	}
	
}

/*---------------------------------------------------------------------------*/
/* This is the application's entry-point.                                    */
/*---------------------------------------------------------------------------*/
int main (int argc, char *argv[])
{
    int  portNum;
    char tempBuf[256] = {0};
    char portNumStr[32];
	
	char d=-5;
	d=!d;
	
	double v = -5.0;
	int s = v*100;
	s = s & 0xffff;
	
	
    
    if (InitCVIRTE (0, argv, 0) == 0)
        return -1;
    if ((g_hmainPanel = LoadPanel(0, "client.uir", MAINPNL)) < 0)
        goto Done;
    DisableBreakOnLibraryErrors();
	
	
	//genera las se�ales 
	
	/*SinePattern (2000, 10.0, 0.0, 10.0, dParte);
	addSignal(dParte, 2000);
	
	WhiteNoise (2000, .5, 1, dRuido);
	for (int i=0;i<2000;i++)
		dParte[i]+=dRuido[i];
	
	addSignal(dParte, 2000);
	
	double dFase=0;
	
	TriangleWave (2000, 15, 0.005, &dFase, dParte);
	addSignal(dParte, 2000);
	
	SawtoothWave (2000, 10, .005, &dFase, dParte);
	addSignal(dParte, 2000);
	
	SquareWave (2000, 15, 0.005, &dFase, 50.0, dParte);
	addSignal(dParte, 2000); */
	
	FileToArray ("c:\\Users\\Public\\Documents\\National Instruments\\CVI2013\\samples\\tcp\\datos2.txt",
				 dVector, VAL_DOUBLE, TOTAL, 1, VAL_GROUPS_TOGETHER, VAL_GROUPS_AS_ROWS, VAL_ASCII);
	
	dVector[0]=dVector[2];
	dVector[1]=dVector[2];
	
	//for (int i=0;i<TOTAL; i++) {
//		dVector[i]+=3.0;
//		dVector[i]*=100.0;
//	}
	
    /* Prompt for the name of the server to connect to */
    /*PromptPopup ("Server Name?",
                 "Type the name of the machine running the sample server "
                 "application.\n\n(example: abc.xyz.com or xxx.xxx.xxx.xxx)",
                 tempBuf, 255);
	  */
    /* Prompt for the port number on the server */		  //200018
    /*PromptPopup ("Port Number?",
                 "Type the port number that was set when starting the sample "
                 "server application.\n\n(example: 10000)",
                 portNumStr, 31);
    portNum = atoi (portNumStr);
	*/
	//Fmt(tempBuf,"%s<%s","172.16.65.133");
	Fmt(tempBuf,"%s<%s","cidesi.cloudapp.net"); 
	portNum = 5000;
    /* Attempt to connect to TCP server... */
    SetWaitCursor (1);
    if (ConnectToTCPServer (&g_hconversation, portNum, tempBuf, ClientTCPCB,
                            NULL, 5000) < 0)
        MessagePopup("TCP Client", "Connection to server failed !");
	
	//if (ConnectToTCPServer (&g_hconversation, 5000, "172.16.66.152", ClientTCPCB,
      //                      NU
        {
        SetWaitCursor (0);
        g_connected = 1;
        
        /* We are successLL, 5000) < 0)
       // MessagePopup("TCP Client", "Connection to server failed !");
    else				 fully connected -- gather info |�||*/
        SetCtrlVal (g_hmainPanel, MAINPNL_CONNECTED, 1);
  		if (GetTCPHostAddr (tempBuf, 256) >= 0)
        	SetCtrlVal (g_hmainPanel, MAINPNL_CLIENT_IP, tempBuf);
        //if (GetTCPHostName (tempBuf, 256) >= 0)
	    //    SetCtrlVal (g_hmainPanel, MAINPNL_CLIENT_NAME, tempBuf);
        tcpChk (GetTCPPeerAddr (g_hconversation, tempBuf, 256));
        SetCtrlVal (g_hmainPanel, MAINPNL_SERVER_IP, tempBuf);
        //tcpChk (GetTCPPeerName (g_hconversation, tempBuf, 256));
        //SetCtrlVal (g_hmainPanel, MAINPNL_SERVER_NAME, tempBuf);
        
        /* display the panel and run the UI */
        DisplayPanel (g_hmainPanel);
        SetActiveCtrl (g_hmainPanel, MAINPNL_STRING);
		//SetCtrlAttribute (g_hmainPanel, MAINPNL_TIMER, ATTR_ENABLED, 1);
        RunUserInterface ();
        }
    
Done:
    /* Disconnect from the TCP server */
    if (g_connected)
    	DisconnectFromTCPServer (g_hconversation);

    /* Free resources and return */
    DiscardPanel (g_hmainPanel);
    CloseCVIRTE ();
    return 0;
}

/*---------------------------------------------------------------------------*/
/* When the user hits ENTER after typing some text, send it to the server... */
/*---------------------------------------------------------------------------*/
int CVICALLBACK TransmitCB (int panelHandle, int controlID, int event,
                            void *callbackData, int eventData1, int eventData2)
{
    char    transmitBuf[512] = {0};

    switch (event)
        {
        case EVENT_COMMIT:
            GetCtrlVal (panelHandle, MAINPNL_STRING, transmitBuf);
            strcat (transmitBuf, "");
            SetCtrlVal (panelHandle, MAINPNL_TRANSMIT, transmitBuf);
            SetCtrlVal (panelHandle, MAINPNL_STRING, "");
            
			
			if (ClientTCPWrite (g_hconversation, transmitBuf,
                                strlen (transmitBuf), 1000) < 0)
                SetCtrlVal (panelHandle, MAINPNL_TRANSMIT,
                            "Transmit Error\n");
            break;
        }
    return 0;
}

/*---------------------------------------------------------------------------*/
/* This is the TCP client's TCP callback.  This function will receive event  */
/* notification, similar to a UI callback, whenever a TCP event occurs.      */
/* We'll respond to the DATAREADY event and read in the available data from  */
/* the server and display it.  We'll also respond to DISCONNECT events, and  */
/* tell the user when the server disconnects us.                             */
/*---------------------------------------------------------------------------*/
int CVICALLBACK ClientTCPCB (unsigned handle, int event, int error,
                             void *callbackData)
{
    char receiveBuf[256] = {0};
    ssize_t dataSize         = sizeof (receiveBuf) - 1;

    switch (event)
        {
        case TCP_DATAREADY:
            if ((dataSize = ClientTCPRead (g_hconversation, receiveBuf,
                                           dataSize, 1000))
                < 0)
                {
                SetCtrlVal (g_hmainPanel, MAINPNL_RECEIVE, "Receive Error\n");
                }
            else
            	{
            	receiveBuf[dataSize] = '\0';
                SetCtrlVal (g_hmainPanel, MAINPNL_RECEIVE, receiveBuf);
                }
            break;
        case TCP_DISCONNECT:
            //MessagePopup ("TCP Client", "Server has closed connection!");
            SetCtrlVal (g_hmainPanel, MAINPNL_CONNECTED, 0);
            g_connected = 0;
            MainPanelCB (0, EVENT_CLOSE, 0, 0, 0);
            break;
    }
    return 0;
}

/*---------------------------------------------------------------------------*/
/* Respond to the UI and clear the receive screen for the user.              */
/*---------------------------------------------------------------------------*/
int CVICALLBACK ClearScreenCB (int panel, int control, int event,
                               void *callbackData, int eventData1,
                               int eventData2)
{
    if (event == EVENT_COMMIT)
        ResetTextBox (panel, MAINPNL_RECEIVE, "");
    return 0;
}

/*---------------------------------------------------------------------------*/
/* Respond to the panel closure to quit the UI loop.                         */
/*---------------------------------------------------------------------------*/
int CVICALLBACK MainPanelCB (int panel, int event, void *callbackData,
                             int eventData1, int eventData2)
{
    if (event == EVENT_CLOSE)
        QuitUserInterface (0);
    return 0;
}


/*---------------------------------------------------------------------------*/
/* Report TCP Errors if any                         						 */
/*---------------------------------------------------------------------------*/
static void ReportTCPError(void)
{
	if (g_TCPError < 0)
		{
		char	messageBuffer[1024];
		sprintf(messageBuffer, 
			"TCP library error message: %s\nSystem error message: %s", 
			GetTCPErrorString (g_TCPError), GetTCPSystemErrorString());
		MessagePopup ("Error", messageBuffer);
		g_TCPError = 0;
		}
}
/*---------------------------------------------------------------------------*/



int CVICALLBACK GenerarDatos (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	//static int iModo=0;		//controla que se ejecute solo 1 vez
	
	static int iIndice=0;
	static int iBloque=TOTAL;
	static int iPaquete=1;
	char cDatos[5000]={0};
	char cMsg[100]={0};			//vector que contiene la informaciona a enviar al servidor
	static int iContador=0;
	static int iTipoSensor=0;	//indica el tipo de sensor que se envia
	static int dVerifica=0;
	static int iSentido=0;
	static int iAcumulador=0;
	static int iTCPMensajes=0;
	
	switch (event)
	{
		case EVENT_TIMER_TICK:

			
			//if (iTipoSensor == 5)
				iTipoSensor=0;
				
			switch (iTipoSensor)
			{
				case 0:			//ECG
					cMsg[0]='E';
					cMsg[1]='C';
					cMsg[2]='G';
					break;
				case 1:			//TAR
					cMsg[0]='T';
					cMsg[1]='A';
					cMsg[2]='R';
					break;
				case 2:			//CAP
					cMsg[0]='C';
					cMsg[1]='A';
					cMsg[2]='P';
					break;
				case 3:			//SPO
					cMsg[0]='S';
					cMsg[1]='P';
					cMsg[2]='O';
					break;
				case 4:			//TMP
					cMsg[0]='T';
					cMsg[1]='M';
					cMsg[2]='P';
					break;
				
			}
			
			iTipoSensor++;
			
			int iCanal=3;
			//comienza a procesar informacion para enviarla por TCP
			if ((iPaquete+iIndice)==iBloque)
			{
				iIndice=0;	
			}
			
			double dOffset = 0.0;
			//double dValor = dVector[iIndice]*65535.0/8.0;//(dVector[iIndice]*10*65535)/8.0; 
			double dValor = (dVector[iIndice]*4.0+5.0)*100.0;
			
			int iValor = dValor;
			iValor = iValor & 0xffff;
			
			
			//dValor = dVerifica*10.0;
			if (iSentido==0)
				dVerifica++;
			else
				dVerifica--;
			
			if (dVerifica==80)
				iSentido=1;
			
			if (dVerifica==0)
				iSentido=0;
			
			DebugPrintf ("%f  ", iValor);
				
			//for (int i=0;i<iPaquete; i++)
			for (int i=3;i<PAQUETE;i+=2)		//controla el numero de canal que agrega al vector de datos
			{									
				//se pueden enviar
				dValor+=dOffset;
				double dAjustado = TruncateRealNumber(dValor);
				
				
				//Rodrigo Sanchez
				//442-2756461
				
				//int iValor = (int)dAjustado;
				
				
				//printf ("\n %d ", iValor);
				
				char  cBaja = (iValor & 0xFF00)>>8;
				char  cAlta = iValor & 0x00FF;
				
			
				
				//if (i == 0)
				//{
					//printf ("\n %d --> %d", cAlta, cBaja); 
				//}
				
				//asigna el dato en el vector de salida
				cMsg[iCanal] = cAlta;
				cMsg[iCanal+1] = cBaja;
				
				iCanal+=2;
				
				dOffset+=0.0;
				
			
				/*char cValor[40]={0};
				Fmt(cValor, "%s<%f", dValor);
			
				strcat(cDatos, cValor);
				strcat(cDatos, ","); */
			}
			iIndice++;
			/*
			
			char dat=0x01;
			iCanal=3;
			cMsg[iCanal++]=dat++;
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++; 
			cMsg[iCanal++]=dat++;    */
			
			
			
			
			//En este punto ya se tiene un paquete de datos completo
			//Ahora hay que duplicar el mensaje hasta N veces el paquete
			
			for (int j=1;j<1;j++) {
				for (int k=0;k<PAQUETE;k++){
					cMsg[j*PAQUETE+k]=cMsg[k];
				}
				cMsg[PAQUETE*j+3]=j+5;
			
			}
			
			//cMsg[19]='F';
		
		//	if (iTCPMensajes < 49)
			{
			
				if (iTCPMensajes == 48)
					iTCPMensajes;
				
				if (ClientTCPWrite (g_hconversation, cMsg, 19, 1000) < 0)
	                SetCtrlVal (panel, MAINPNL_TRANSMIT,
	                            "Transmit Error\n");
        	
				//else
				{
					iContador;
				}
				 iContador++;
				 
			} 
			//else
			{
				//SetCtrlAttribute (g_hmainPanel, MAINPNL_TIMER, ATTR_ENABLED, 0);

			}
			
			iTCPMensajes++;
	}
	return 0;
}
