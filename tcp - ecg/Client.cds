<distribution version="13.0.2" name="Client" type="MSI">
	<prebuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></prebuild>
	<postbuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></postbuild>
	<msi GUID="{E97FF16B-45E7-4F6F-A7CE-7631415D7482}">
		<general appName="Client" outputLocation="c:\Users\Public\Documents\National Instruments\CVI2013\samples\tcp\cvidistkit.Client" relOutputLocation="cvidistkit.Client" outputLocationWithVars="c:\Users\Public\Documents\National Instruments\CVI2013\samples\tcp\cvidistkit.%name" relOutputLocationWithVars="cvidistkit.%name" upgradeBehavior="1" autoIncrement="true" version="1.0.2">
			<arp company="CIDESI" companyURL="" supportURL="" contact="" phone="" comments=""/>
			<summary title="" subject="" keyWords="" comments="" author=""/></general>
		<userinterface language="English" showPaths="true" readMe="" license="">
			<dlgstrings welcomeTitle="Client" welcomeText=""/></userinterface>
		<dirs appDirID="101">
			<installDir name="Client" dirID="100" parentID="7" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Program Files]" dirID="2" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="Client" dirID="101" parentID="2" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Start&gt;&gt;Programs]" dirID="7" parentID="-1" isMSIDir="true" visible="true" unlock="false"/></dirs>
		<files>
			<simpleFile fileID="0" sourcePath="c:\Users\Public\Documents\National Instruments\CVI2013\samples\tcp\cvibuild.client\Release\client.exe" targetDir="101" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/></files>
		<fileGroups>
			<projectOutput targetType="0" dirID="101" projectID="0">
				<fileID>0</fileID></projectOutput>
			<projectDependencies dirID="101" projectID="0"/></fileGroups>
		<shortcuts>
			<shortcut name="Client" targetFileID="0" destDirID="100" cmdLineArgs="" description="" runStyle="NORMAL"/></shortcuts>
		<mergemodules/>
		<products/>
		<runtimeEngine installToAppDir="false" activeXsup="true" analysis="true" cvirte="true" dotnetsup="true" instrsup="true" lowlevelsup="true" lvrt="true" netvarsup="true" rtutilsup="true">
			<hasSoftDeps/></runtimeEngine><sxsRuntimeEngine>
			<selected>false</selected>
			<doNotAutoSelect>false</doNotAutoSelect></sxsRuntimeEngine>
		<advanced mediaSize="650">
			<launchConditions>
				<condition>MINOS_WINXP</condition>
			</launchConditions>
			<includeConfigProducts>true</includeConfigProducts>
			<maxImportVisible>silent</maxImportVisible>
			<maxImportMode>merge</maxImportMode>
			<custMsgFlag>false</custMsgFlag>
			<custMsgPath>msgrte.txt</custMsgPath>
			<signExe>false</signExe>
			<certificate></certificate>
			<signTimeURL></signTimeURL>
			<signDescURL></signDescURL></advanced>
		<Projects NumProjects="1">
			<Project000 ProjectID="0" ProjectAbsolutePath="c:\Users\Public\Documents\National Instruments\CVI2013\samples\tcp\client.prj" ProjectRelativePath="client.prj"/></Projects>
		<buildData progressBarRate="0.143243936272893">
			<progressTimes>
				<Begin>0.000000000000000</Begin>
				<ProductsAdded>0.057860500000000</ProductsAdded>
				<DPConfigured>3.932860500000000</DPConfigured>
				<DPMergeModulesAdded>6.675360749999999</DPMergeModulesAdded>
				<DPClosed>23.175362000000003</DPClosed>
				<DistributionsCopied>27.796496250000001</DistributionsCopied>
				<End>698.109829999999990</End></progressTimes></buildData>
	</msi>
</distribution>
